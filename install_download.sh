
mkdir target
cd target
wget http://kent.dl.sourceforge.net/project/jboss/JBoss/JBoss-5.1.0.GA/jboss-5.1.0.GA-jdk6.zip
unzip jboss-5.1.0.GA-jdk6.zip
wget http://build.open-esb.net:8080/jenkins/job/openesb-jboss/lastSuccessfulBuild/artifact/packaging/jboss-dist/target/openesb-jboss5-dist-2.4.0-SNAPSHOT-distribution.zip
unzip openesb-jboss5-dist-2.4.0-SNAPSHOT-distribution.zip
rm -fr jboss-5.1.0.GA/server/all
rm -fr jboss-5.1.0.GA/server/minimal
rm -fr jboss-5.1.0.GA/server/standard
rm -fr jboss-5.1.0.GA/server/web
mv jboss5-jbi-install/ jboss-5.1.0.GA
mv openesb-jboss5-deployer.jar jboss-5.1.0.GA/server/default/deployers/
mv openesb-jboss5.sar jboss-5.1.0.GA/server/default/deploy
rm jboss-5.1.0.GA-jdk6.zip
rm openesb-jboss5-dist-1.1-distribution.zip
mkdir jboss-5.1.0.GA/server/default/jbi
mkdir jboss-5.1.0.GA/server/default/jbi/autoinstall
mkdir jboss-5.1.0.GA/server/default/jbi/autodeploy
cp ../install/logger.properties jboss-5.1.0.GA/server/default/conf
cp ../install/run.sh jboss-5.1.0.GA/bin
cp ../install/run.bat jboss-5.1.0.GA/bin
wget http://build.open-esb.net:8080/jenkins/view/JBoss/job/slf4j-jboss-logging/lastSuccessfulBuild/artifact/target/slf4j-jboss-logging-1.1.0.Final-SNAPSHOT.jar
rm jboss-5.1.0.GA/common/lib/slf4j-jboss-logging.jar
mv slf4j-jboss-logging-1.1.0.Final-SNAPSHOT.jar jboss-5.1.0.GA/common/lib/
wget http://repo1.maven.org/maven2/org/slf4j/slf4j-api/1.7.5/slf4j-api-1.7.5.jar
rm jboss-5.1.0.GA/common/lib/slf4j-api.jar
mv slf4j-api-1.7.5.jar jboss-5.1.0.GA/common/lib/
mkdir components
cd components
wget http://build.open-esb.net:8080/jenkins/job/openesb-components-git/ws/ojc-core/encodersl/packaging/bld/encoderlib-installer-2.4.0-SNAPSHOT.jar
wget http://build.open-esb.net:8080/jenkins/job/openesb-components-git/ws/ojc-core/wsdlextsl/packaging/bld/wsdlextlib-installer-2.4.0-SNAPSHOT.jar	
wget http://build.open-esb.net:8080/jenkins/job/openesb-core-git/ws/runtime/wsdl/wsdlsl/bld/wsdlsl-2.4.0-SNAPSHOT.jar
wget http://build.open-esb.net:8080/jenkins/job/openesb-components-git/lastSuccessfulBuild/artifact/dist/bpelse.jar
wget http://build.open-esb.net:8080/jenkins/job/openesb-components-git/lastSuccessfulBuild/artifact/dist/databasebc.jar
wget http://build.open-esb.net:8080/jenkins/job/openesb-components-git/lastSuccessfulBuild/artifact/dist/emailbc.jar
wget http://build.open-esb.net:8080/jenkins/job/openesb-components-git/lastSuccessfulBuild/artifact/dist/schedulerbc.ja
wget http://build.open-esb.net:8080/jenkins/job/openesb-components-git/lastSuccessfulBuild/artifact/dist/filebc.jar
wget http://build.open-esb.net:8080/jenkins/job/openesb-components-git/lastSuccessfulBuild/artifact/dist/ftpbc.jar
wget http://build.open-esb.net:8080/jenkins/job/openesb-components-git/ws/ojc-core/httpsoapbc/packaging-full/bld/httpbc-installer-full-2.4.0-SNAPSHOT.jar
wget http://build.open-esb.net:8080/jenkins/job/openesb-components-git/lastSuccessfulBuild/artifact/dist/jdbcbc.jar
wget http://build.open-esb.net:8080/jenkins/job/openesb-components-git/lastSuccessfulBuild/artifact/dist/jmsbc.jar
wget http://build.open-esb.net:8080/jenkins/job/openesb-components-git/lastSuccessfulBuild/artifact/dist/pojose.jar
wget http://build.open-esb.net:8080/jenkins/job/openesb-components-git/lastSuccessfulBuild/artifact/dist/restbc.jar
wget http://build.open-esb.net:8080/jenkins/job/openesb-components-git/lastSuccessfulBuild/artifact/dist/xsltse.jar
cd ../..
mv target openesb-jboss-2.4.0-SNAPSHOT
tar cvfz openesb-jboss.tar.gz ./openesb-jboss-2.4.0-SNAPSHOT




